# 1. Variables y tipos



Hola, soy Angelo. Tal vez me recuerdes en peliculas como la clase inicial mas larga de historia y la venganza de los lenguajes de programación en 3D.

Estoy ansioso por comenzar la parte asincrona de nuestro curso, la cual consiste en tal como mencionamos, el estudio personal de los contenidos que ya vimos en la clase presencial. Obviamente esta sección del curso apunta a que practiquemos y formemos el conocimiento directamente metiendo las manos al barro, que segun mi perspectica es la mejor forma de iniciarse en este mundo y seguir perfeccionando los conocimientos ya adquiridos.

Primero que todo, para aquellos que no tengan los contenidos en video dejare los videos aca.

### Introducción y pequeña historia de la informatica.

[Clase grabada - Parte 1](https://www.twitch.tv/videos/673005576)

### Variables, tipos y nuestro primeros pasos en la programación.

[Clase grabada - Parte 2](https://www.twitch.tv/videos/673005576?t=02h18m40s)

# Como funciona esto (¿Nunca habias entrado a un repositorio?)

Existe una alta probabilidad que nunca hallas visto un lugar como este. A pesar que tendremos una clase exclusivamente para que te hagas el mejor amigo de este tipo de software, te adelantare que desde ahora en adelante sera una parte fundamental de tu carrera IT, ya que es una especie de cajon donde guardaras y mantendras a salvo todo tu codigo de cualquier amenaza (Cafes voladores, gatos locos y apocalipsis del fin del mundo). 

En los repositorios, nos aseguramos de que nuestros codigos fuentes, no solo este a salvo, sino que nos permite trabajar de forma colaborativa con otras personas, compartiendo nuestro avances, asi como obteniendo los avances de otros en un mismo proyecto de software, pero lo genial no acaba aca, cada commit (Lllamaremos commit a cada cambio que una persona como tu sube a este repositorio) queda guardado como una foto instantanea del proyecto, permitiendo volver y recuperar cada uno de estos cambios en cualquier momento que queramos, incluso si en una etapa posterior estos cambios ya fueron borrados. 

Quiero que piense en los repositorios  y GIT (La herramienta que usamos de repositorio), es una especie de Google Drive con esteroides diseñado para facilitarnos el trabajo, ya que tiene integración con nuestros editores como Visual Studio Code.

# Ok, no tengo idea de lo que me hablas, pero suena interesante y quiero acción!

Pues bien, se que estas ansioso por practicar lo que revisamos en clases y para esto hay una serie de archivos con ejercicos, los cuales iras haciendo uno a uno. Espero disfruten de este curso tanto como yo. Y los invito a dar lo mejor de cada uno de ustedes.

Mucho exito en el nuevo desafio que comenzaron.

![img](https://media0.giphy.com/media/fnyo4ks4wLLHVTupNZ/giphy.gif?cid=ecf05e47etqxrl5s4gzyzs37rg1w1isqxpz1g6ac1mq75of7&rid=giphy.gif)